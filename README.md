# Ansible Role: vaultwarden-docker

Ansible role to create vaultwarden docker container.

## Requirements

No special requirements; note that this role requires root access, so either run it in a playbook with a global `become: yes`, or invoke the role in your playbook like:

    - hosts: pwsafe
      roles:
        - role: wreiner.vaultwarden-docker
          become: yes

## Dependencies

This role depends on installed and configured docker container runtime.

## Role Variables

The version must be specified with:

```
bitwardenrs_docker__bitwardenrs_version: 1.18.0
```

At the moment only SSL encrypted database connections to PostgreSQL is supported.

```
vaultwarden__dbuser: "bitwarden"
vaultwarden__dbpass: "{{ postgresql__bitwarden_password }}"
vaultwarden__dbhost: "192.168.A.B"
vaultwarden__dbname: "bitwarden"
```

The interface to which the port should be exposed must be sepcified.

```
vaultwarden__listen_address: "192.168.X.Y"
```

The port exposed will be 38080 on the specified interface.

It is possible to allow/disallow signups:

```
vaultwarden__signups_allowed: "false"
```

The admin token is needed to access the admin area.

```
vaultwarden__admin_token: "{{ bitwarden_admin_token }}"
```

Generate the admin token with:

```
openssl rand -base64 48
```

vaultwarden also wants to send mail:

```
vaultwarden__smtp_host: "smtp.example.com"
vaultwarden__smtp_from: "from@example.com"
vaultwarden__smtp_from_name:
vaultwarden__smtp_port: "587"
vaultwarden__smtp_host: "true"
vaultwarden__smtp_security: "starttls"
vaultwarden__smtp_username: "user@example.com"
vaultwarden__smtp_password: "{{ bitwarden_smtp_pass }}"
vaultwarden__domain: "https://vault.yourdomain.com"
```

To force new users to verify their email address upon registration:

```
vaultwarden__signups_verify: "true"
```

Org admins can send invitations even when signups is disallowed:

```
vaultwarden__invitations_allowed: "true"
```

Should the password hint be shown:

```
vaultwarden__show_password_hint: "true"
```

Header of the invitation email:

```
vaultwarden__invitation_org_name: "Vault Name"
```

Running behind an Apache 2 reverse proxy we need to set the right header to see the client ip in the successful login email:

```
vaultwarden__ip_header: "X-Forwarded-For"
```

In Apache 2 the module needs to be enabled and the RemoteIPHeader configuration to be set:

```
RemoteIPHeader X-Forwarded-For
```

## Sources & Docs

- [vaultwarden env template](https://github.com/dani-garcia/vaultwarden/blob/master/.env.template)
